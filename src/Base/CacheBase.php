<?php

namespace Ruiadr\Cache\Base;

use Ruiadr\Cache\Base\Interface\CacheBaseInterface;

abstract class CacheBase implements CacheBaseInterface
{
    /**
     * @param bool $bypass true pour outrepasser le mécanisme de cache
     */
    public function __construct(private bool $bypass = false)
    {
    }

    final public function getByPass(): bool
    {
        return $this->bypass;
    }

    final public function setBypass(bool $bypass): CacheBaseInterface
    {
        $this->bypass = $bypass;

        return $this;
    }

    final public function __set(string $name, string $value): void
    {
        $this->set($name, $value);
    }

    final public function __get(string $name): mixed
    {
        return $this->get($name);
    }

    final public function __isset(string $name): bool
    {
        return $this->exists($name);
    }

    final public function __unset(string $name): void
    {
        $this->set($name, null);
    }
}
