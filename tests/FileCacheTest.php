<?php

namespace Ruiadr\Fetcher\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Cache\Base\Interface\CacheBaseInterface;
use Ruiadr\Cache\Exception\FileCacheException;
use Ruiadr\Cache\FileCache;
use Ruiadr\Cache\Interface\FileCacheInterface;

final class FileCacheTest extends TestCase
{
    public function __destruct()
    {
        $this->getFileCache()->deferredPurge();
    }

    private function getDirectoryCache(): string
    {
        return dirname(__FILE__).'/cache';
    }

    private function getFileCache(): FileCacheInterface
    {
        return new FileCache($this->getDirectoryCache());
    }

    public function testBuild(): void
    {
        $cache = $this->getFileCache();

        $this->assertInstanceOf(CacheBaseInterface::class, $cache);
    }

    public function testBuildError(): void
    {
        $this->expectException(FileCacheException::class);
        new FileCache('/file_cache_exception');
    }

    public function testSimpleGetters(): void
    {
        $cache = $this->getFileCache();

        $this->assertSame($cache->getPath(), $this->getDirectoryCache());
    }

    public function testSetGet(): void
    {
        $cache = $this->getFileCache();

        $cache->set('test_set_get1', 'test_set_get1');
        $this->assertSame('test_set_get1', $cache->get('test_set_get1'));
        $this->assertNull($cache->get('test_set_get2'));

        $cache->set('test_set_get3', '');
        $this->assertNull($cache->get('test_set_get3'));

        $cache->set('', 'test_set_get4');
        $this->assertNull($cache->get(''));

        $cache->set('test_set_get5', 'test_set_get5');
        $this->assertTrue($cache->exists('test_set_get5'));

        $cache->set('test_set_get6', null);
        $this->assertFalse($cache->exists('test_set_get6'));
    }

    public function testExists(): void
    {
        $cache = $this->getFileCache();

        $cache->set('test_exists1', 'test_exists1');

        $this->assertTrue($cache->exists('test_exists1'));
        $this->assertFalse($cache->exists('test_exists2'));
        $this->assertFalse($cache->exists(''));
    }

    public function testTtl(): void
    {
        $cache = $this->getFileCache();

        $cache->set('test_ttl1', 'test_ttl1', 1);
        $this->assertTrue($cache->exists('test_ttl1'));

        sleep(2);

        $this->assertFalse($cache->exists('test_ttl1'));
    }

    public function testByPass(): void
    {
        $cache = $this->getFileCache();

        $cache->set('test_bypass1', 'test_bypass1');
        $cache->set('test_bypass2', 'test_bypass2');

        $cache->setBypass(true);

        $this->assertFalse($cache->exists('test_bypass1'));
        $this->assertFalse($cache->exists('test_bypass2'));

        $cache->setBypass(false);

        $this->assertTrue($cache->exists('test_bypass1'));
        $this->assertTrue($cache->exists('test_bypass2'));
    }

    public function testMagicsMethods(): void
    {
        $cache = $this->getFileCache();

        $cache->test_magics1 = 'test_magics1';

        $this->assertSame('test_magics1', $cache->test_magics1);
        $this->assertTrue(isset($cache->test_magics1));
        $this->assertFalse(isset($cache->test_magics2));

        unset($cache->test_magics1);
        $this->assertFalse(isset($cache->test_magics1));
    }

    public function testRemove(): void
    {
        $cache = $this->getFileCache();

        $cache->set('test_remove1', 'test_remove1');
        $this->assertTrue($cache->remove('test_remove1'));

        $cache->set('test_remove2', 'test_remove2');
        $cache->set('test_remove3', 'test_remove3');
        $this->assertTrue($cache->remove(['test_remove2', 'test_remove3']));

        $cache->set('test_remove4', 'test_remove4');
        $this->assertFalse($cache->remove(['test_remove4', 'test_remove5']));
    }

    public function testDeferredPurge(): void
    {
        $cache = $this->getFileCache();

        $cache->test_deferred_purge = 'test_deferred_purge';
        $this->assertSame('test_deferred_purge', $cache->test_deferred_purge);

        // La purge est différée à la destruction de l'objet
        // de cache, donc les données doivent être encore accessibles.
        $cache->deferredPurge();

        $cache->test_deferred_purge = 'test_deferred_purge';
        $this->assertSame('test_deferred_purge', $cache->test_deferred_purge);

        unset($cache); // La destruction de l'objet doit bien vider le cache.

        $cache = $this->getFileCache();

        $this->assertFalse($cache->exists('test_deferred_purge'));
    }

    public function testFlush(): void
    {
        $cache = $this->getFileCache();

        $this->assertTrue($cache->flush());
    }
}
