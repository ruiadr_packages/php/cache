<?php

namespace Ruiadr\Cache\Interface;

use Ruiadr\Cache\Base\Interface\CacheBaseInterface;

interface FileCacheInterface extends CacheBaseInterface
{
    /**
     * Retourne le chemin $path qui a servi à la construction de l'objet courant.
     *
     * @return string Chemin de stockage du cache
     */
    public function getPath(): string;

    /**
     * Supprime le cache courant.
     * La suppression est différée à la destruction de l'objet.
     */
    public function deferredPurge(): void;
}
