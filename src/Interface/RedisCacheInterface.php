<?php

namespace Ruiadr\Cache\Interface;

use Ruiadr\Cache\Base\Interface\CacheBaseInterface;

interface RedisCacheInterface extends CacheBaseInterface
{
    final public const DEFAULT_HOST = '127.0.0.1';
    final public const DEFAULT_PORT = 6379;

    final public const STATUS_OK = 'OK';

    /**
     * Retourne l'hôte qui a servi à la construction de l'objet.
     *
     * @return string Hôte du cache
     */
    public function getHost(): string;

    /**
     * Retourne le port qui a servi à la construction de l'objet.
     *
     * @return int Port du cache
     */
    public function getPort(): int;

    /**
     * Retourne la base de données qui a servi à la construction de l'objet.
     *
     * @return int Base de données du cache
     */
    public function getDatabase(): int;
}
