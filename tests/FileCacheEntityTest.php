<?php

namespace Ruiadr\Fetcher\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Base\File\File;
use Ruiadr\Cache\FileCache;
use Ruiadr\Cache\FileCacheEntity;
use Ruiadr\Cache\Interface\FileCacheEntityInterface;
use Ruiadr\Cache\Interface\FileCacheInterface;

final class FileCacheEntityTest extends TestCase
{
    public function __destruct()
    {
        $this->getFileCache()->deferredPurge();
    }

    private function getDirectoryCache(): string
    {
        return dirname(__FILE__).'/cache';
    }

    private function getFileCache(): FileCacheInterface
    {
        return new FileCache($this->getDirectoryCache());
    }

    public function testSanitizeKey(): void
    {
        $this->assertSame('cachefilename', FileCacheEntity::sanitizeKey('  Câche  File  Näme  '));
    }

    public function testGenerateFilename(): void
    {
        $expected = 'cachefilename'.FileCacheEntityInterface::FILE_EXTENSION;
        $this->assertSame($expected, FileCacheEntity::generateFilename('  Câche  File  Näme  '));
    }

    public function testGenerateKey(): void
    {
        $filepath = $this->getDirectoryCache().'/'.FileCacheEntity::generateFilename('test_gen_key');
        $file = File::createTree($filepath);

        $this->assertSame('testgenkey', FileCacheEntity::generateKey($file));
    }

    public function testEntity(): void
    {
        $name = 'test_entity';

        $filepath = $this->getDirectoryCache().'/'.FileCacheEntity::generateFilename($name);
        $file = File::createTree($filepath);

        $key = FileCacheEntity::sanitizeKey($name);

        $fileCacheEntity = new FileCacheEntity($file->getDirectory(), $key);

        $fileCacheEntity->setValue('value_of_test_entity');
        $this->assertSame(0, $fileCacheEntity->getTTL());
        $this->assertSame('value_of_test_entity', $fileCacheEntity->getValue());
    }
}
