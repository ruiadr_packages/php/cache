<?php

namespace Ruiadr\Cache;

use Ruiadr\Base\File\Interface\DirectoryInterface;
use Ruiadr\Base\File\Interface\FileInterface;
use Ruiadr\Cache\Interface\FileCacheEntityInterface;
use Ruiadr\Utils\StringUtils;

final class FileCacheEntity implements FileCacheEntityInterface
{
    private ?FileInterface $file = null;
    private ?array $structuredData = null;

    /**
     * Création d'une instance de cache FileCacheEntityInterface
     * dans le répertoire $directory identifié par la clé $key.
     *
     * @param DirectoryInterface $directory Répertoire utilisé pour stocker les fichiers de cache
     * @param string             $key       La clé qui identifie les données à stocker
     */
    public function __construct(
        private readonly DirectoryInterface $directory,
        private readonly string $key
    ) {
    }

    public static function sanitizeKey(string $key): string
    {
        return strtolower(StringUtils::sanitizeAlnum(StringUtils::replaceSpecialChars($key)));
    }

    public static function generateFilename(string $key): string
    {
        return self::sanitizeKey($key).FileCacheEntityInterface::FILE_EXTENSION;
    }

    public static function generateKey(FileInterface $file): string
    {
        return self::sanitizeKey($file->getBasename(FileCacheEntityInterface::FILE_EXTENSION));
    }

    /**
     * Instancier un FileInterface qui sera utilisé pour stocker les données
     * à mettre en cache.
     *
     * @return FileInterface Fichier de cache
     */
    private function getFile(): FileInterface
    {
        return $this->file ??= $this->directory->createFile(self::generateFilename($this->key));
    }

    /**
     * Les éléments stockés sont :
     *      - FileCacheEntityInterface::KEY_TTL : durée de vie du cache
     *      - FileCacheEntityInterface::KEY_VALUE : la valeur mise en cache
     *
     * Lorsque la clé $key vaut l'une des deux constantes listées ci-dessus, alors
     * la valeur associée est directement retournée.
     *
     * Peut retourner null si la clé $key ne correspond à aucune des constantes
     * listées ci-dessus. La méthode étant privée, il est peu probable que
     * $key soit erroné.
     */
    private function getStructuredData(string $key): array|string|null
    {
        $file = $this->getFile();

        $content = null;
        if ($file instanceof FileInterface) {
            $content = $file->read();
        }

        $unserializedContent = null;
        if (is_string($content)) {
            $unserializedContent = unserialize($content);
        }

        if (is_array($unserializedContent)) {
            $this->structuredData = $unserializedContent;
        }

        $structuredData = null;

        if (is_array($this->structuredData) && array_key_exists($key, $this->structuredData)) {
            $structuredData = $this->structuredData[$key];
        }

        return $structuredData;
    }

    public function getTTL(): ?int
    {
        return $this->getStructuredData(FileCacheEntityInterface::KEY_TTL);
    }

    public function expired(): bool
    {
        $ttl = $this->getTTL();

        return !is_int($ttl) || ($ttl > 0 && time() > $ttl);
    }

    public function getValue(bool $purgeOnExpiry = true): ?string
    {
        if (!$this->expired()) {
            return $this->getStructuredData(FileCacheEntityInterface::KEY_VALUE);
        }

        if ($purgeOnExpiry) {
            $this->setValue(null);
        }

        return null;
    }

    public function setValue(?string $value = null, int $ttl = 0): bool
    {
        $result = false;

        $file = $this->getFile();
        if ($file instanceof FileInterface) {
            if (null === $value) { // Une suppression du cache est requise ?
                $removed = $file->remove();

                if ($removed) {
                    // Uniquement si la suppression s'est bien déroulée,
                    // sans quoi on pourrait se retrouver avec une incohérence
                    // entre les données stockées et celles qui se trouvent
                    // en mémoire.
                    $this->file = null;
                    $this->structuredData = null;
                }

                $result = $removed;
            } else {
                $data = [
                    FileCacheEntityInterface::KEY_TTL => $ttl <= 0 ? 0 : time() + $ttl,
                    FileCacheEntityInterface::KEY_VALUE => $value,
                ];

                $written = $file->write(serialize($data), false);

                if ($written) {
                    // On en profite pour mettre à jour les données qui se trouvent
                    // en mémoire avec celles qu'on vient tout juste de stocker.
                    $this->structuredData = $data;
                }

                $result = $written;
            }
        }

        return $result;
    }
}
