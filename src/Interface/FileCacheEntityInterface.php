<?php

namespace Ruiadr\Cache\Interface;

use Ruiadr\Base\File\Interface\FileInterface;

interface FileCacheEntityInterface
{
    final public const KEY_TTL = 'ttl';
    final public const KEY_VALUE = 'value';

    final public const FILE_EXTENSION = '.cache';

    /**
     * Nettoyage de la clé $key passée en paramètre pour servir
     * d'identifiant de cache, et aussi de nom de fichier de cache.
     *
     * @param string $key La clé à nettoyer
     *
     * @return string La clé nettoyée
     */
    public static function sanitizeKey(string $key): string;

    /**
     * Génère un nom de fichier de cache (extension comprise) en
     * partant de la clé $key passée en paramètre.
     *
     * Ex: ' Héllo ' : 'hello.cache'
     *
     * @param string $key La clé
     *
     * @return string Le nom de fichier généré
     */
    public static function generateFilename(string $key): string;

    /**
     * Génère une clé servant d'identifiant de cache à partir d'une
     * instance FileInterface.
     *
     * Ex: 'hello.cache' : 'hello'
     *
     * @param FileInterface $file L'instance du fichier
     *
     * @return string La clé générée
     */
    public static function generateKey(FileInterface $file): string;

    /**
     * Retourne le TTL de l'élément mis en cache.
     * Si le TTL n'a pas pu être trouvé, la méthode retourne null.
     *
     * @return ?int Le TTL ou null s'il n'a pas été trouvé
     */
    public function getTTL(): ?int;

    /**
     * Retourne true si le cache a expiré ou que le TTL associé
     * n'a pas été trouvé.
     *
     * @return bool true si le cache a expiré
     */
    public function expired(): bool;

    /**
     * Retourne la valeur en cache associée à la clé qui a servi
     * à construire l'objet courant.
     * Avec le paramètre $purgeOnExpiry à true: si le cache a expiré,
     * la méthode déclenche automatiquement la suppression du fichier
     * qui a servi pour le stockage des données.
     * Si une valeur en cache a été trouvée, et qu'elle n'a pas encore
     * expiré, elle est retournée, sinon la méthode retourne null.
     *
     * @param bool $purgeOnExpiry true pour déclencher la suppression du fichier de cache
     *                            lorsqu'il est considéré comme expiré
     *
     * @return ?string La valeur en cache ou null s'il n'y a rien de valide à exploiter
     */
    public function getValue(bool $purgeOnExpiry = true): ?string;

    /**
     * Définir une valeur à cacher de $ttl secondes.
     * Lorsque le $ttl passé en paramètre est <= 0, le cache a une durée
     * de vie illimitée dans le temps.
     * Si $value vaut null, alors la clé associée au cache est supprimée. En réalité
     * c'est le fichier qui a servi au stockage des données qui est supprimé.
     * La méthode retourne true si le traitement attendu a pu aboutir, false sinon.
     *
     * @param ?string $value La valeur à cacher ou null pour supprimer une clé existante
     * @param int     $ttl   La durée de vie du cache exprimée en secondes
     *
     * @return bool true si le traitement a pu aboutir, false sinon
     */
    public function setValue(?string $value = null, int $ttl = 0): bool;
}
