<?php

namespace Ruiadr\Cache\Base\Interface;

interface CacheBaseInterface
{
    /**
     * @return bool true si le cache doit être outrepassé, false sinon.
     *              Dans les deux cas, le système utilisé par le cache doit
     *              être fonctionnel, par exemple avec RedisCache le serveur
     *              doit rester joignable.
     */
    public function getByPass(): bool;

    /**
     * Lorsque la méthode est utilisée avec le paramètre $bypass à true,
     * la méthode CacheBaseInterface::exists() retourne toujours false.
     *
     * @param bool $bypass true pour outrepasser le cache, false pour le laisser
     *                     fonctionner normalement
     *
     * @return CacheBaseInterface L'instance du cache
     */
    public function setBypass(bool $bypass): CacheBaseInterface;

    /**
     * Ajouter un élément $value au cache, identifié par le paramètre $key,
     * et pour une durée de $ttl secondes.
     * Lorsque $ttl est <= 0, la clé n'expire jamais.
     * $key et $value ne doivent pas être des chaînes vides, sans quoi l'ajout
     * au cache échouera, aucune erreur ne sera générée.
     *
     * @param string  $key   La clé qui identifie la valeur à cacher
     * @param ?string $value La valeur à cacher, null pour retirer la clé du cache
     * @param int     $ttl   La durée de vie dans le cache
     *
     * @return CacheBaseInterface L'instance du cache
     */
    public function set(string $key, ?string $value = null, int $ttl = 0): CacheBaseInterface;

    /**
     * Récupère du cache le contenu de la clé $key passée en paramètre.
     * Si la clé n'est pas trouvée (n'a jamais existé ou a expiré),
     * la méthode retourne null.
     *
     * @param string $key La clé qui identifie la valeur à récupérer
     *
     * @return ?string La valeur stockée ou null si la clé n'est pas trouvée
     */
    public function get(string $key): ?string;

    /**
     * Retourne true si la clé $key existe, false sinon.
     * Si la clé $key a expiré, la méthode retourne false.
     * Lorsque la méthode "setBypass()" est utilisée avec true,
     * la méthode "exists()" retournera alors toujours false.
     * Cela permet de désactiver l'utilisation du cache par les
     * applications sous-jacentes durant les phases de développement.
     * Le système de cache doit néanmoins rester actif, sans quoi une exception
     * pourrait être levée.
     *
     * @param string $key La clé qui identifie la valeur à tester
     *
     * @return bool true si la clé existe, false dans le cas contraire
     */
    public function exists(string $key): bool;

    /**
     * Vide le cache.
     *
     * @return bool true si le cache a correctement été vidé,
     *              false dans le cas contraire
     */
    public function flush(): bool;

    /**
     * Supprime une ou plusieurs clés.
     * La méthode retourne true si toutes les clés passées dans $keys
     * ont bien été supprimées, false si au moins une clé n'a pas été
     * supprimée, par exemple si elle n'existait pas.
     *
     * @param string|array $keys Les clés à retirer du cache
     *
     * @return bool true si toutes les clés ont été retirées du cache, false sinon
     */
    public function remove(string|array $keys): bool;

    /**
     * Stocker dans le cache une valeur $value associée à la
     * clé $name. La donnée est stockée indéfiniment dans le cache.
     *
     * @param string $name  Clé utilisée pour identifier la valeur en cache
     * @param string $value Valeur à stocker
     */
    public function __set(string $name, string $value): void;

    /**
     * Récupérer une valeur en cache identifiée par le paramètre $name.
     *
     * @param string $name Clé utilisée pour identifier la valeur en cache
     *
     * @return mixed La valeur stockée ou null si rien n'a été trouvé
     */
    public function __get(string $name): mixed;

    /**
     * Déterminer si une clé existe ou non en cache.
     *
     * @param string $name Clé utilisée pour identifier la valeur en cache
     *
     * @return bool true si la clé existe, false sinon
     */
    public function __isset(string $name): bool;

    /**
     * Supprimer une clé du cache.
     *
     * @param string $name Clé utilisée pour identifier la valeur en cache
     */
    public function __unset(string $name): void;
}
