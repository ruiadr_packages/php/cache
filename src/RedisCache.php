<?php

namespace Ruiadr\Cache;

use Predis\Client;
use Ruiadr\Cache\Base\CacheBase;
use Ruiadr\Cache\Exception\RedisCacheException;
use Ruiadr\Cache\Interface\RedisCacheInterface;

final class RedisCache extends CacheBase implements RedisCacheInterface
{
    private ?Client $redis = null;

    /**
     * Construction de l'objet de cache pour Redis.
     * En cas de problème, une exception de type RedisCacheException est levée.
     *
     * @param string $host     Hôte qui héberge le cache
     * @param int    $port     Port de connexion au cache
     * @param int    $database Numéro de la base de données à utiliser, pour que la valeur soit
     *                         prise en compte, elle doit être >= 0
     * @param bool   $bypass   Outrepasser le cache
     *
     * @throws RedisCacheException
     */
    public function __construct(
        private readonly string $host = RedisCacheInterface::DEFAULT_HOST,
        private readonly int $port = RedisCacheInterface::DEFAULT_PORT,
        private readonly int $database = -1,
        bool $bypass = false)
    {
        parent::__construct($bypass);

        $parameters = [
            'scheme' => 'tcp',
            'host' => $this->host,
            'port' => $this->port,
        ];

        $options = [];

        if ($this->database > -1) {
            $options = [
                'parameters' => [
                    'database' => $this->database,
                ],
            ];
        }

        try {
            $this->redis = new Client($parameters, $options);
            // On force la connexion dès la construction de l'objet pour générer
            // une RedisCacheException en cas d'erreur.
            $this->redis->connect();
        } catch (\Exception $e) {
            throw new RedisCacheException($e);
        }
    }

    public function __destruct()
    {
        $this->redis->disconnect();
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function getPort(): int
    {
        return $this->port;
    }

    public function getDatabase(): int
    {
        return $this->database;
    }

    public function set(string $key, ?string $value = null, int $ttl = 0): RedisCacheInterface
    {
        if (is_string($value)) {
            $key = trim($key);
            $value = trim($value);

            if (strlen($key) > 0 && strlen($value) > 0) {
                $this->redis->set($key, $value);

                if ($ttl > 0) {
                    $this->redis->expire($key, $ttl);
                }
            }
        } else {
            // Une valeur à null doit déclencher une suppression de la clé du cache.
            $this->remove($key);
        }

        return $this;
    }

    public function get(string $key): ?string
    {
        return $this->redis->get($key);
    }

    public function exists(string $key): bool
    {
        // Lorsque getByPass() retourne true, alors la méthode exists() doit
        // toujours retourner false, cf CacheBaseInterface::exists().
        return !$this->getByPass() && $this->redis->exists($key);
    }

    public function flush(): bool
    {
        $payload = $this->redis->flushdb()->getPayload();

        return is_string($payload) && RedisCacheInterface::STATUS_OK === trim($payload);
    }

    public function remove(string|array $keys): bool
    {
        if (is_string($keys)) {
            $keys = [$keys];
        }

        // La méthode "del()" retourne le nombre de clés effectivement supprimées.
        return $this->redis->del($keys) === count($keys);
    }
}
