<?php

namespace Ruiadr\Fetcher\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Cache\Base\Interface\CacheBaseInterface;
use Ruiadr\Cache\Exception\RedisCacheException;
use Ruiadr\Cache\Interface\RedisCacheInterface;
use Ruiadr\Cache\RedisCache;

final class RedisCacheTest extends TestCase
{
    public function __destruct()
    {
        $this->getRedisCache()->flush();
    }

    private static function getHost(): string
    {
        $testingHost = getenv('REDIS_HOST');
        if (!is_string($testingHost)) {
            $testingHost = RedisCacheInterface::DEFAULT_HOST;
        }

        return $testingHost;
    }

    private function getRedisCache(int $database = -1): RedisCacheInterface
    {
        return new RedisCache($this->getHost(), RedisCacheInterface::DEFAULT_PORT, $database);
    }

    public function testBuild(): void
    {
        $cache = $this->getRedisCache();

        $this->assertInstanceOf(CacheBaseInterface::class, $cache);
    }

    public function testBuildErrorHost(): void
    {
        $this->expectException(RedisCacheException::class);
        new RedisCache('helloworld');
    }

    public function testBuildErrorPort(): void
    {
        $this->expectException(RedisCacheException::class);
        new RedisCache($this->getHost(), 1234);
    }

    public function testBuildErrorDatabase(): void
    {
        $this->expectException(RedisCacheException::class);
        new RedisCache($this->getHost(), RedisCacheInterface::DEFAULT_PORT, 9999);
    }

    public function testSimpleGetters(): void
    {
        $cache = $this->getRedisCache();

        $this->assertSame($cache->getHost(), $this->getHost());
        $this->assertSame($cache->getPort(), RedisCacheInterface::DEFAULT_PORT);
        $this->assertSame($cache->getDatabase(), -1);
    }

    public function testSetGet(): void
    {
        $cache = $this->getRedisCache();

        $cache->set('test_set_get1', 'test_set_get1');
        $this->assertSame('test_set_get1', $cache->get('test_set_get1'));
        $this->assertNull($cache->get('test_set_get2'));

        $cache->set('test_set_get3', '');
        $this->assertNull($cache->get('test_set_get3'));

        $cache->set('', 'test_set_get4');
        $this->assertNull($cache->get(''));

        $cache->set('test_set_get5', 'test_set_get5');
        $this->assertTrue($cache->exists('test_set_get5'));

        $cache->set('test_set_get6', null);
        $this->assertFalse($cache->exists('test_set_get6'));
    }

    public function testExists(): void
    {
        $cache = $this->getRedisCache();

        $cache->set('test_exists1', 'test_exists1');

        $this->assertTrue($cache->exists('test_exists1'));
        $this->assertFalse($cache->exists('test_exists2'));
        $this->assertFalse($cache->exists(''));
    }

    public function testTtl(): void
    {
        $cache = $this->getRedisCache();

        $cache->set('test_ttl1', 'test_ttl1', 1);
        $this->assertTrue($cache->exists('test_ttl1'));

        sleep(2);

        $this->assertFalse($cache->exists('test_ttl1'));
    }

    public function testDatabases(): void
    {
        $cache_db0 = $this->getRedisCache(0);
        $cache_db1 = $this->getRedisCache(1);

        $cache_db0->set('test_db0', 'test_db0');
        $this->assertTrue($cache_db0->exists('test_db0'));

        $cache_db1->set('test_db1', 'test_db1');
        $this->assertTrue($cache_db1->exists('test_db1'));

        $this->assertFalse($cache_db0->exists('test_db1'));
        $this->assertFalse($cache_db1->exists('test_db0'));
    }

    public function testByPass(): void
    {
        $cache = $this->getRedisCache();

        $cache->set('test_bypass1', 'test_bypass1');
        $cache->set('test_bypass2', 'test_bypass2');

        $cache->setBypass(true);

        $this->assertFalse($cache->exists('test_bypass1'));
        $this->assertFalse($cache->exists('test_bypass2'));

        $cache->setBypass(false);

        $this->assertTrue($cache->exists('test_bypass1'));
        $this->assertTrue($cache->exists('test_bypass2'));
    }

    public function testMagicsMethods(): void
    {
        $cache = $this->getRedisCache();

        $cache->test_magics1 = 'test_magics1';

        $this->assertSame('test_magics1', $cache->test_magics1);
        $this->assertTrue(isset($cache->test_magics1));
        $this->assertFalse(isset($cache->test_magics2));

        unset($cache->test_magics1);
        $this->assertFalse(isset($cache->test_magics1));
    }

    public function testRemove(): void
    {
        $cache = $this->getRedisCache();

        $cache->set('test_remove1', 'test_remove1');
        $this->assertTrue($cache->remove('test_remove1'));

        $cache->set('test_remove2', 'test_remove2');
        $cache->set('test_remove3', 'test_remove3');
        $this->assertTrue($cache->remove(['test_remove2', 'test_remove3']));

        $cache->set('test_remove4', 'test_remove4');
        $this->assertFalse($cache->remove(['test_remove4', 'test_remove5']));
    }

    public function testFlush(): void
    {
        $cache = $this->getRedisCache();

        $this->assertTrue($cache->flush());
    }
}
