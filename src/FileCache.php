<?php

namespace Ruiadr\Cache;

use Ruiadr\Base\File\Directory;
use Ruiadr\Cache\Base\CacheBase;
use Ruiadr\Cache\Exception\FileCacheException;
use Ruiadr\Cache\Interface\FileCacheEntityInterface;
use Ruiadr\Cache\Interface\FileCacheInterface;

final class FileCache extends CacheBase implements FileCacheInterface
{
    private ?Directory $directory = null;
    private ?array $caches = null;
    private bool $requiredPurge;

    /**
     * Construction de l'objet de cache fichier.
     * En cas de problème, une exception de type FileCacheException est levée.
     *
     * @param string $path   Répertoire où stocker le cache
     * @param bool   $bypass Outrepasser le cache
     *
     * @throws FileCacheException
     */
    public function __construct(
        private readonly string $path,
        bool $bypass = false)
    {
        parent::__construct($bypass);

        $this->requiredPurge = false;

        try {
            $this->directory = Directory::create($this->path);
        } catch (\Exception $e) {
            throw new FileCacheException($e);
        }
    }

    public function __destruct()
    {
        if ($this->requiredPurge) {
            $this->flush();
            $this->directory->remove();
        }
    }

    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Retourne l'instance de "FileCacheEntityInterface" associée à la
     * clé $key passée en paramètre. Si l'instance n'existe pas, elle
     * est créée avant d'être stockée en mémoire.
     *
     * @param string $key Clé qui identifie le fichier de cache
     *
     * @return FileCacheEntityInterface Instance de l'entité de cache
     */
    private function getFileCacheEntity(string $key): FileCacheEntityInterface
    {
        $this->caches ??= [];
        $key = FileCacheEntity::sanitizeKey($key);

        return $this->caches[$key] ??= new FileCacheEntity($this->directory, $key);
    }

    /**
     * Retourne les fichiers de cache qui ont été générés
     * dans le répertoire de stockage.
     *
     * @return array Les fichiers de cache
     */
    private function getFiles(): array
    {
        return $this->directory->listFiles('*'.FileCacheEntityInterface::FILE_EXTENSION);
    }

    public function set(string $key, ?string $value = null, int $ttl = 0): FileCacheInterface
    {
        if (is_string($value)) {
            $key = trim($key);
            $value = trim($value);

            if (strlen($key) > 0 && strlen($value) > 0) {
                $this->getFileCacheEntity($key)->setValue($value, $ttl);
            }
        } else {
            $this->getFileCacheEntity($key)->setValue($value);
            unset($this->caches[$key]);
        }

        return $this;
    }

    public function get(string $key): ?string
    {
        return $this->getFileCacheEntity($key)->getValue();
    }

    public function exists(string $key): bool
    {
        // Lorsque getByPass() retourne true, alors la méthode exists() doit
        // toujours retourner false, cf CacheBaseInterface::exists().
        return !$this->getByPass() && is_string($this->get($key));
    }

    public function flush(): bool
    {
        $keys = array_map([FileCacheEntity::class, 'generateKey'], $this->getFiles());

        return $this->remove($keys);
    }

    public function remove(string|array $keys): bool
    {
        if (is_string($keys)) {
            $keys = [$keys];
        }

        $countErrors = 0;

        foreach ($keys as $key) {
            // Pour rester cohérent avec la définition de la méthode, on se doit
            // de comptabiliser en erreur une clé qui ne se trouve pas dans le cache.
            if (!$this->exists($key)) {
                ++$countErrors;
                continue;
            }

            $this->set($key, null);
        }

        return 0 === $countErrors;
    }

    public function deferredPurge(): void
    {
        $this->requiredPurge = true;
    }
}
