# ruiadr/cache

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=ruiadr_packages_cache&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=ruiadr_packages_cache)

Fonctionnalités de base pour gérer des systèmes de cache.

## Tests

Lancer les tests unitaires :

```shell
php vendor/bin/phpunit
```

Lancer les tests unitaires avec des statistiques de couverture du code :

La commande aura pour effet de générer un fichier **coverage.xml** qui pourra ensuite être utilisé par [SonarCloud](https://sonarcloud.io/project/overview?id=ruiadr_packages_cache), et un répertoire **coverage** contenant le HTML permettant de consulter le compte rendu depuis son navigateur.

```shell
php vendor/bin/phpunit --log-junit=tests.xml --coverage-clover=coverage.xml --coverage-html coverage
```

**xdebug** doit être installé et activé. Exemple avec un conteneur Docker :

```shell
# /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

zend_extension=xdebug.so

[xdebug]
xdebug.mode=coverage
```

## Général

Ce paquet est actuellement capable de gérer 2 implémentations de cache :

- Via Redis : Classe **RedisCache**
- Via des fichiers : Classe **FileCache**

L'instanciation sera donc différente en fonction du système de cache à utiliser, mais l'utilisation en devient ensuite identique.

## Cache Redis

```php
use Ruiadr\Cache\RedisCache;

$redisCache = new RedisCache();
```

Paramètres du constructeur :

- string **$host** : Hôte qui héberge le service (défaut : '127.0.0.1')
- int **$port** : Port pour accéder au service (défaut : 6379)
- int **$database** : Base de données à utiliser (défaut : -1 = ne pas spécifier de valeur lors de la connexion à Redis)
- bool **$bypass** : Bypass le mécanisme de cache, le service doit être néanmoins disponible (défaut : false)

Au besoin, toutes les valeurs sont accessibles via des getters :

```php
$redisCache->getHost();      // string("127.0.0.1")
$redisCache->getPort();      // int(6379)
$redisCache->getDatabase();  // int(-1)
$redisCache->getByPass();    // bool(false)
```

## Cache Fichier

```php
use Ruiadr\Cache\FileCache;

$fileCache = new FileCache('<PATH>/cache');
```

Paramètres du constructeur :

- string **$path** : Répertoire dans lequel stocker le cache
- bool **$bypass** : Bypass le mécanisme de cache (défaut : false)

Au besoin, toutes les valeurs sont accessibles via des getters :

```php
$fileCache->getPath();      // string("<PATH>/cache")
$fileCache->getByPass();    // bool(false)
```

Une méthode permet de lancer une purge complète du cache fichier, le cache est physiquement supprimé, et uniquement à la destruction de l'objet pour éviter les effets de bord : **deferredPurge()**.

```php
$fileCache->deferredPurge();
```

## Utilisation du cache

Dans les 2 cas, l'utilisation du cache est ensuite identique.

Un setter permet de redéfinir la valeur du bypass à la volée : **setBypass(bool $bypass)**.

### Ajouter une clé en cache

```php
$cache->set('test1', 'hello');    // Définir une clé
```

Par défaut cette clé est mise en cache sans TTL, elle n'expire donc jamais. Il est possible de personnaliser le TTL d'une clé exprimé en secondes :

```php
$cache->set('test1', 'hello', 3600);    // Expire au bout d'une heure
```

### Récupérer une clé en cache

```php
$cache->set('test1', 'hello');

$cache->get('test1');    // Retourne string("hello")
$cache->get('test2');    // Retourne NULL
```

### Tester la présence d'une clé en cache

```php
$cache->set('test1', 'hello');

$cache->exists('test1');    // Retourne bool(true)
$cache->exists('test2');    // Retourne bool(false)
```

### Supprimer une clé en cache

```php
$cache->set('test1', 'hello');

$cache->remove('test1');    // Retourne bool(true)
$cache->remove('test2');    // Retourne bool(false)
```

Il est possible de supprimer plusieurs clés d'un coup, pour cela il suffit de passer un tableau à la méthode. Elle retournera **bool(true)** si toutes les clés ont bien été supprimées, **bool(false)** si au moins une clé n'a pas pu être supprimée (ex : elle n'existait pas).

```php
$cache->set('test1', 'hello');
$cache->set('test2', 'world');

$cache->remove(['test1', 'test2']);             // Retourne bool(true)
$cache->remove(['test1', 'test2', 'test3']);    // Retourne bool(false)
```

### Vider le cache

```php
$cache->flush();    // Retourne bool(true) en cas de succès, bool(false) sinon
```

### Les propriétés

Pour des cas simplifiés d'utilisation du cache, il est possible de travailler directement avec les propriétés. Cette approche fonctionne avec toutes les implémentations de cache.

Exemple avec Redis :

```php
use Ruiadr\Cache\RedisCache;

$cache = new RedisCache();

$cache->test1 = "hello";   // Ajout d'une clé en cache, durée de vie illimitée
echo $cache->test1;        // Affiche string("hello")
isset($cache->test1);      // Retourne bool(true)
unset($cache->test1);      // Supprime la clé du cache
```
